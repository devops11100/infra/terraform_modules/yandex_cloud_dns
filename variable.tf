variable "dns-zone-name" {
  type        = string
  description = "DNS Zone name"
}

variable "dns-zone-description" {
  type        = string
  description = "DNS Zone description"
}

variable "dns-zone-labels" {
  type        = map(any)
  description = "DNS Zone description"
  default = {
    "stand" = "stage"
  }
}

variable "dns-zone" {
  type        = string
  description = "DNS Zone description"
}

variable "dns-zone-public" {
  type        = bool
  description = "DNS Zone public"
}

variable "dns-private-networks" {
  type        = list(any)
  description = "DNS Private Networks"
  nullable    = true
  default     = null
}

variable "dns-record-name" {
  type        = string
  description = "DNS Record name"
}

variable "dns-record-type" {
  type        = string
  description = "DNS Record type"
}

variable "dns-record-ttl" {
  type        = number
  description = "DNS Record ttl"
}

variable "dns-record-data" {
  type        = list(string)
  description = "DNS Record data"
}