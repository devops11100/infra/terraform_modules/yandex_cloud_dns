resource "yandex_dns_zone" "zone" {
  name        = var.dns-zone-name
  description = var.dns-zone-description

  labels = {
    "created" = "terraform"
    "stand"   = var.dns-zone-labels["stand"]
  }

  zone             = var.dns-zone
  public           = var.dns-zone-public
  private_networks = var.dns-private-networks
}

resource "yandex_dns_recordset" "rec" {
  zone_id = yandex_dns_zone.zone.id
  name    = var.dns-record-name
  type    = var.dns-record-type
  ttl     = var.dns-record-ttl
  data    = var.dns-record-data
}